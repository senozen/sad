const api_url = 'http://localhost/SAD/api';

var data = $.ajax({
    url: api_url + '/cabang',
    success: function (data) {
        var content = '',
            n = 0;
        $.each(data, function (index, val) {
            n++;
            content += '<tr><td>' + n +
                '</td><td>' + val.kd_cabang +
                '</td><td>' + val.name_cabang +
                '</td><td>' + val.alamat +
                '</td><td>' + val.kecamatan +
                '</td><td>' + val.kota +
                '</td><td>' + val.provinsi +
                '</td><td>' + val.pimpinan +
                '</td><td>' + val.jabatan +
                '</td><td>' + val.hp +
                '</td><td>' + val.email +
                '</td><td>' + val.aktif +
                '</td></tr>';
        });
        $(".list_cabang tbody").html(content);
    }
});