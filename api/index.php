<?php
// output akan dikeluarkan sebagai json
header("Content-Type:application/json");

//hasil akan masuk ke dalam variabel output
$output = [];
// semua proses akan dilakukan di dalam file data/index.php dan akan dimasukkan dalam variabel $output
include "data/index.php";

//hasil dari proses diatas akan dihasilkan sebuah json yang diisi data yang tersimpan dalam variabel  
echo json_encode($output);
